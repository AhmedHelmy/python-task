from django.core.management.base import BaseCommand

from ...services import decoder

class Command(BaseCommand):
    help = 'Displays decrypted string'

    def add_arguments(self, parser):
        parser.add_argument('string', type=str, help='Indicates the string to be decrypted')

    
    def handle(self, *args, **kwargs):
        string = kwargs['string']
        request = decoder.Decoder.excute(string)
        self.stdout.write("the decrypted string of %a is '%s'" % (string,request))