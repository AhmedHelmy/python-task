from django.core.management.base import BaseCommand

from ...services import encoder

class Command(BaseCommand):
    help = 'Displays encrypted string'

    def add_arguments(self, parser):
        parser.add_argument('string', type=str, help='Indicates the string to be encrypted')

    
    def handle(self, *args, **kwargs):
        string = kwargs['string']
        request = encoder.Encoder.excute(string)
        self.stdout.write("the encrypted string of %a is '%s'" % (string,request))