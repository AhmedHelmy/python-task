from django.core.management.base import BaseCommand

from ...services import decoder
from ...services import encoder

class Command(BaseCommand):
    help = 'Displays decrypted string'

    def add_arguments(self, parser):
        parser.add_argument('method', type=str, help='Indicates the method to be used')
        parser.add_argument('string', type=str, help='Indicates the string to be encrypted or decrypted')

    
    def handle(self, *args, **kwargs):
        method = kwargs['method']
        string = kwargs['string']
        if(method == "Encrypt"):
            request = encoder.Encoder.excute(string)
            self.stdout.write("the encrypted string of %a is '%s'" % (string,request))
        elif(method == "Decrypt"):
            request = decoder.Decoder.excute(string)
            self.stdout.write("the decrypted string of %a is '%s'" % (string,request))
        else:
            self.stdout.write("Invalid method name please use Encrypt or Decrypt")
        