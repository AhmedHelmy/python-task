from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from .services.encoder import *
from .services.decoder import *

# Create your views here.

@api_view(['POST'])
def encode(request):
	string = request.data['string']
	return JsonResponse(Encoder.excute(string), safe=False)


@api_view(['POST'])
def decode(request):
	string = request.data['string']
	return JsonResponse(Decoder.excute(string), safe=False)