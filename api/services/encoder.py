
class Encoder(object):
	@staticmethod
	def excute(word):
		s = 3
		result = ""

		
   # transverse the plain text
		for char in word:

			# Encrypt uppercase characters in plain text
			if (char.isupper()):
				result += chr((ord(char) + s-65) % 26 + 65)

			# Encrypt lowercase characters in plain text
			elif (char.islower()):
				result += chr((ord(char) + s - 97) % 26 + 97)
			# add other characters (int or spaces)	
			else:
				result += char
				
		return result