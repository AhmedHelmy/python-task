from django.test import TestCase
from ..services import encoder


class encoderTest(TestCase):
    def test_encoder_method(self):
        request = encoder.Encoder.excute("Hello World")
        self.assertEqual(request, "Khoor Zruog")